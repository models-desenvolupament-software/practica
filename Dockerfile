FROM adoptopenjdk:11.0.8_10-jre-hotspot
ARG JAR_FILE
COPY ${JAR_FILE} greetings.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/greetings.jar"]