pipeline {
    agent any
    environment {
        APP_VERSION = 'feature'
        DOCKER_REGISTRY_URL = 'https://hub.docker.com/'
        DOCKER_REGISTRY_CRED = 'docker-hub-credentials'
        DOCKER_REGISTRY_REPO = "${env.DOCKER_REGISTRY_REPO}"
        DOCKER_IMAGE = ''
        JAR_FILE = './build/libs/exercise-0.0.1-SNAPSHOT.jar'
    }
    stages {
        stage('Build') {
            steps {
                sh "./gradlew clean build"
            }
        }
        stage('Docker') {
            when {
                anyOf {
                    branch 'master'
                    branch 'release-*'
                }
            }
            steps {
                script {
                    if (env.BRANCH_NAME.equals("master")) {
                        APP_VERSION = 'master'
                    }
                    else if (env.BRANCH_NAME.startsWith('release-')) {
                        APP_VERSION = env.BRANCH_NAME.split("-")[1]
                    }
                    else {
                        APP_VERSION = 'feature'
                    }
                }
                script {
                    docker.withRegistry('', DOCKER_REGISTRY_CRED) {
                        docker.build(DOCKER_REGISTRY_REPO, "--build-arg JAR_FILE=" + JAR_FILE + " .").push(APP_VERSION)
                    }
                }
            }
        }
        stage('Deploy to k8s') {
            when {
                anyOf {
                    branch 'release-*'
                }
            }
            steps {
                script {
                    echo(DOCKER_REGISTRY_REPO)
                    echo(APP_VERSION)
                    sh "cat ./kubernetes/hello-world-deployment.yml | sed 's@{{DOCKER_REGISTRY_REPO}}@" + DOCKER_REGISTRY_REPO + "@g' | " +
                    "sed 's/{{TAG_VERSION}}/" + APP_VERSION + "/g' | kubectl apply -f -"
                    sh "cat ./kubernetes/hello-world-service.yml | kubectl apply -f -"
                }
            }
        }
    }
    post {
        always {
            junit '**/test-results/test/*.xml'

            publishCoverage adapters: [jacocoAdapter('build/reports/jacoco/test/jacocoTestReport.xml')]
        }
    }
}
