package com.mdas.sdm.exercise.infrastructure;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GreetingsRestController.class)
@TestPropertySource(properties = {"greetings.message = Hello World!!"})
class GreetingsRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getGreetings() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/greetings"))
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World!!"));
    }
}