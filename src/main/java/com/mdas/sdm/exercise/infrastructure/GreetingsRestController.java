package com.mdas.sdm.exercise.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingsRestController {

    @Value("${greetings.message:Hello World!!}")
    private String greetingMessage;

    @GetMapping("/greetings")
    public String getGreetings() {
        return greetingMessage;
    }
}
